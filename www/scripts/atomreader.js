﻿function atomfeedViewModel() {
    var self = this;

    self.Items = ko.observableArray();

    self.addItem = function (item) {
        self.Items.push(item);
        console.log("atomfeedViewModel pushed new item " + item.title);
        
    };

    self.removeItem = function (item) {
        self.Items.remove(item);
    };
};


atomreader = function () { };

atomreader.getFeed = function (url, koObservableArray) {
    var items = [];
    var thresh = 70;
	jQuery.get(url, function (data) {
	    $(data).find("entry").each(function () {
			var el = $(this);
			var item = [];
			item.title = stripHTML($(el).find("title").text());
			//item.name = item.title;
			//item.author = $(el).find("author").text();
			item.description = stripHTML($(el).find("summary").text());
			if (item.description.length > thresh) {
			    item.short = item.description.substr(0, thresh) + " ... read more";
			} else {
			    item.short = item.description;
			}
			item.content = $(el).find("content").text();
			item.link = $(el).find("link").attr("href");
			item.pubDate = $(el).find("pubDate").text();
			item.Date = new Date(item.pubDate);
		    //koObservableArray.push(item);
			
			koObservableArray.addItem(item);
			//items.push(item);
		});
		
	});
	//cb(items);
};
