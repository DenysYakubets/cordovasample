﻿function feedViewModel() {
    var self = this;

    self.Items = ko.observableArray();

    self.addItem = function (item) {
        self.Items.push(item);
        console.log("feedViewModel pushed new item " + item.title);
        
    };

    self.removeItem = function (item) {
        self.Items.remove(item);
    };
};


rssreader = function () { };

rssreader.getFeed = function (url, koObservableArray) {
    var items = [];
	jQuery.get(url, function (data) {
		$(data).find("item").each(function () {
			var el = $(this);
			var item = [];
			item.title = stripHTML($(el).find("title").text());
			//item.name = item.title;
			//item.author = $(el).find("author").text();
			item.content = stripHTML(item.description = $(el).find("description").text());
			if (item.description.length > 20) {
			    item.short = item.description.substr(1, 20);
			} else {
			    item.short = item.description;
			}
			
			item.link = $(el).find("link").text();
			item.pubDate = $(el).find("pubDate").text();
			item.Date = new Date(item.pubDate);
		    //koObservableArray.push(item);
			
			koObservableArray.addItem(item);
			//items.push(item);
		});
		
	});
	//cb(items);
};
