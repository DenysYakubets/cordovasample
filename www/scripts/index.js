﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

var emergencyAlertItems = new atomfeedViewModel();
var newsItems = new atomfeedViewModel();
var concillorItems = new atomfeedViewModel();
var eventItems = new atomfeedViewModel();


(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        $("#tab-4").hide();
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
        
        // RSS Feeds

        
//        loadScreen('main.html', function(){
            ko.applyBindings(emergencyAlertItems, document.getElementById("alerts"));
            ko.applyBindings(newsItems, document.getElementById("newsitems"));
            ko.applyBindings(concillorItems, document.getElementById("councillorItems"));
            ko.applyBindings(eventItems, document.getElementById("eventItems"));
            atomreader.getFeed("https://www.wheatlandcounty.ca/rss-feeds-listing/emergency-alerts", emergencyAlertItems)
            atomreader.getFeed("https://www.wheatlandcounty.ca/rss-feeds-listing/news-events", newsItems);
            atomreader.getFeed("https://www.wheatlandcounty.ca/rss-feeds-listing/councillors", concillorItems);
            atomreader.getFeed("https://www.wheatlandcounty.ca/rss-feeds-listing/events", eventItems);
//        });


    };


    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
} )();

function activateTabItem(tab)
{
    $("#contact_form").hide();
    $("#news_content").show();
    $('.active-item').removeClass('active-item');
    $('.active-submenu-history').removeClass('active-submenu-history');
//    $(tab+'-menuitem').addClass('active-item');
    $('.'+tab+'-menuitem').addClass('active-submenu-history'); 

    $('.activate-'+tab).trigger('click');
    $('.close-sidebar').click();    
    
}

function openContactForm()
{
    $("#contact_form_thanks").hide();
    $("#news_content").hide();
    $(".tab-4-menuitem").addClass('active-submenu-history');
    $("#contact_form").show();
    $('.close-sidebar').click();

}

function doContactFormSubmit()
{
    var from = $("#form_email").val();
    var message = $("#form_message").val();
    var subject = $("#form_subject").val();
    var fullname = $("#form_fullname").val();

    if(from==""){
        $("#contactEmailFieldError").show();
        return;
    }

    if( message==""){
        $("#contactMessageTextareaError").show();
        return;
    }
    
    if(subject==""){
    
    }
    
    var email = {
        fullname: fullname,
        from: from,
        subject: subject,
        message: message,
        k: "FLgkhn5i82904865%"
    };

    
    $.ajax("https://www.wheatlandcounty.ca/_mobileapp/mail.php", {method: "POST", data: email}).done(function(){
        $("#contact_form").hide();
        $("#contact_form_thanks").show();
    });

    return(false);
}
