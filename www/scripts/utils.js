﻿function stripHTML(html) {
    var container = document.createElement('div');
    //var text = document.createTextNode(dirtyString);
    //container.appendChild(text);
    //return container.innerHTML; // innerHTML will be a xss safe string
    container.innerHTML = html;
    return (container.innerText);
}


function showScreen(page)
{
    $(".dyna-content-div").hide();
    $(page).show();
    $('#page-content-scroll').animate({
         scrollTop: 0
    }, 250);

}
